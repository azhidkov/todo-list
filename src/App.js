import React, { useState } from "react";
import Input from "./components/Input";
import Button from "./components/Button";
import TodoList from "./components/TodoList";
import "./styles/App.scss";
import { requests } from "./utils";

const App = () => {
  const [text, setText] = useState("");

  const handleChangeInput = (e) => {
    setText(e.target.value);
  };

  const handleClick = async () => {
    await requests.sendTodo(text);
    setText("");
  };

  return (
    <div className="app">
      <div className="app-container">
        <div className="app-input">
          <Input value={text} onChange={handleChangeInput} />
          <Button onClick={handleClick} />
        </div>
        <TodoList />
      </div>
    </div>
  );
};

export default App;
