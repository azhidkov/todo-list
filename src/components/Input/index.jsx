import React from "react";

import "./Input.scss";

const Input = ({ value, onChange }) => {
  return (
    <>
      <input type="text" value={value} onChange={onChange} />
    </>
  );
};

export default Input;
