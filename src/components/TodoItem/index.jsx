import React, { useState } from "react";
import { requests } from "../../utils";

import "./TodoItem.scss";

const TodoItem = ({ data, elementName }) => {
  const [isChecked, setIsChecked] = useState(data?.isDone || false);

  const handleChangeCheckbox = () => {
    setIsChecked((prev) => !prev);
  };

  const handleDeleteItem = async () => {
    await requests.deleteTodo(elementName);
  };

  return (
    <>
      <div className="todo-item-container">
        <input
          type="checkbox"
          checked={isChecked}
          onChange={handleChangeCheckbox}
        />
        <span
          onClick={handleChangeCheckbox}
          className={`${isChecked ? "checked" : ""}`}
        >
          {data?.title}
        </span>
        <ion-icon name="trash-outline" onClick={handleDeleteItem}></ion-icon>
      </div>
    </>
  );
};

export default TodoItem;
