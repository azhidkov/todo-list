import React, { useEffect, useState } from "react";
import TodoItem from "../TodoItem";
// import { items } from "./mock";
import { requests } from "../../utils";

import "./TodoList.scss";

const TodoList = () => {
  const [todos, setTodos] = useState([]);
  const [keys, setKeys] = useState([]);

  const fetchTodos = async () => {
    const todos = await requests.getTodos();
    setTodos(todos.values);
    setKeys(todos.keys);
  };

  useEffect(() => {
    fetchTodos();
  }, []);

  return (
    <>
      <div className="todo-list-container">
        {todos.map((item, index) => (
          <TodoItem data={item} key={item.id} elementName={keys[index]} />
        ))}
      </div>
    </>
  );
};

export default TodoList;
