import React from "react";

const Button = ({ onClick }) => {
  return (
    <>
      <button onClick={onClick}>Add</button>
    </>
  );
};

export default Button;
