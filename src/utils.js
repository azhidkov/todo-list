import axios from "axios";

const url =
  "https://todo-list-96875-default-rtdb.europe-west1.firebasedatabase.app/todos.json";

export const requests = {
  async getTodos() {
    const response = await axios({
      method: "get",
      url,
    });

    return {
      keys: Object.keys(response.data),
      values: Object.values(response.data),
    };
  },
  async sendTodo(text) {
    await axios({
      method: "post",
      url,
      data: {
        id: Math.round(Math.random() * 1000),
        title: text,
        isDone: false,
      },
    });
  },
  async deleteTodo(name) {
    await axios({
      method: "delete",
      url: `https://todo-list-96875-default-rtdb.europe-west1.firebasedatabase.app/todos/${name}.json`,
    });
  },
};
